# Lab 2: Implement MVT in Django Framework

CSGE602022 - Platform-Based Programming (Pemrograman Berbasis Platform) @
Faculty of Computer Science Universitas Indonesia, Even Semester 2021/2022

---

## Tujuan Pembelajaran

Setelah menyelesaikan tutorial ini, mahasiswa diharapkan untuk mengerti :

- Mengerti menerapkan separation of concern Model View Controller (Model, Template, View menggunakan Django)

---

## Routing App pada Django

Pada `lab_2` ini kalian diharapkan bisa menerapkan routing dengan module atau app pada Django.
Berkas `urls.py` pada direktori ini adalah contoh URLconf yang disediakan oleh Django, yang dapat digunakan untuk
melakukan `routing` ke `apps` Django lainnya. Contoh untuk membuat `routing` ke `app` lain , berdasarkan `lab_2` yang sudah dikerjakan (cek repo):

```python
...
from django.urls import path
import lab_2.urls as lab_2
urlpattern = [
    ...
    path('lab-2/', include(lab_2)),
    ...
]
```

> Note: Tanda titik tiga `...` pada kode di atas sebagai tanda bahwa isinya bisa apa saja, sesuai kebutuhan.

Perhatikan bahwa berkas `urls.py` pada `app` Django tidak dibuat secara otomatis oleh Django, melainkan dibuat secara manual.
Pada contoh di atas, dalam direkotri `lab_2` diasumsikan ada berkas `urls.py`. Tanda dot (titik) sebagai penanda
untuk mengakses isi direktori tersebut.

Penjelasan ringkas:

- Baris kode `path('lab-2/', include(lab_2))`, yang artinya untuk setiap
  URL dengan awalan `lab-2/` yang menangani URL tersebut adalah berkas `lab_2.urls`.
- alamat `http://localhost/lab-2/` akan ditangani oleh berkas `lab_2.urls`
- `import lab_2.urls as lab_2` --> `lab_2.urls` diganti namanya dengan `lab_2`,
  sehingga baris kode `include(lab_2, ... )` tetap memanggil berkas `lab_2.urls`

Selanjutnya untuk melihat penggunaan `views`, kita harus melihat isi berkas dari `lab_2.urls` :

```python
from django.urls import path
from .views import index

urlpatterns = [
    path('', index, name='index'),
]

```

Penjelasan ringkas :

- Baris kode `path('', index, name='index')`, yang artinya untuk URL dengan awalan / akan dialihkan ke sebuah `views` bernama `index`
- `index` juga dapat diganti dengan `views.index` tapi sama saja, fungsinya untuk memproses tampilan

Pada berkas `views.py` diasumsikan (dan seharusnya) ada fungsi bernama `index` atau `def index(request):`.
Pada berkas `views.py` ini, juga akan diatur bagaimana _request_ akan diproses sebelum ditampilkan.
Perhatikan fungsi `render` yang ada pada berkas `views.py`, terdapat berkas HTML.
Direktori `templates` berfungsi untuk menyimpan berkas HTML yang dipanggil oleh `views`, jadi
pastikan berkas tersebut ada dan namanya sesuai untuk menghindari kesalahan.

>
> INGAT: penulisan variabel, parameter, fungsi, dsb, pada Django _case-sensitif_. Jadi harus teliti dalam menulisakan kode
>
> Penjelasan ringkas mengenai URL Django : <cite> <https://tutorial.djangogirls.org/en/django_urls/> </cite>
>
> Penjelasan lebih detail bagaimana URLconf bekerja : <cite> <https://docs.djangoproject.com/en/3.2/topics/http/urls/> </cite>

## Cara Menampilkan _Webpage_

Pada lab ini anda telah disediakan sebuah _template_ `apps` Django. Tugas Anda adalah melengkapi _template_ yang sudah diberikan sesuai dengan yang sudah diajarkan pada sesi kelas berkaitan dengan _MTV Architecture_.

1. Bukalah folder `lab_2` lalu lengkapi variable `mhs_name`, `birth_date`, dan `npm` dalam `views.py`.

2. Lengkapi _attributes_ pada model `TrackerTugas` yang belum ada, yaitu detail dan deadline (tanggal).

3. Daftarkan model `TrackerTugas` pada file `admin.py` di dalam folder `lab_2` sehingga nantinya kita bisa menambahkan entri tugas menggunakan dashboard admin. Untuk menambahkan entri teman menggunakan dashboard admin dapat mengikuti tutorial berikut [ini](https://docs.djangoproject.com/en/3.2/intro/tutorial02/#introducing-the-django-admin). Jalankan command berikut agar kita dapat memasukkan data ke dashboard admin.

    ```python
    python manage.py makemigrations
    python manage.py migrate
    ```

4. Jangan lupa untuk memanggil model `TrackerTugas` yang sudah dibuat pada file `views.py` di dalam folder `lab_2` serta modifikasi file `deadline_tugas_lab2.html` di dalam folder `lab_2/templates` agar dapat menampilkan daftar teman yang sudah kita masukkan melalui dashboard admin.

5. Bukalah file `urls.py` di dalam folder `lab_2`. Disini akan ditulis konfigurasi URL yang akan diproses. Dalam hal ini
    kalian harus menambahkan URL untuk menampilkan halaman list teman dengan menggunakan _template_ yang sudah disediakan.
    Modifikasi `urls.py` agar pada saat _request_ diberikan pada `<HOSTNAME>/lab-2/tugas` maka halaman yang ada pada
    [deadline_tugas_lab2.html](../../lab_2/templates/deadline_tugas_lab2.html) bisa dimunculkan.

6. Jalankan Django secara lokal :

    ```python
    python manage.py runserver
    ```

7. Selamat, kalian sudah berhasil menampilkan `lab_2`

8. `commit` dan `push` pekerjaan kalian ke repo masing - masing

## Checklist

1. Display your profile page:

   1. [ ] Implement TODOs on test.py
   2. [ ] Open views.py on lab_2 folder, implement code in lines 4, 6, and 7.
   3. [ ] Refresh <http://localhost:8000/>
   4. [ ] See your profile on the webpage

2. Create list tugas page:
   1. [ ] Implement TODOs on models.py
   2. [ ] Register your model on admin.py so you can access your database from Django Admin later
   3. [ ] Implement TODOs on urls.py
   4. [ ] Fix unit test related to friend list URL on test.py
   5. [ ] Implement TODOs on views.py
   6. [ ] Implement TODOs on templates/deadline_tugas_lab2.html
   7. [ ] Add your task, homework, or assignment information via Django Admin (see: <https://docs.djangoproject.com/en/1.8/intro/tutorial02/>)
   8. [ ] Access your deadline list URL and see your deadline list information on the page
