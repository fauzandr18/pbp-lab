from django import forms
from lab_3.models import MyWatchList

class WatchlistForm(forms.ModelForm):
    class Meta:
        model = MyWatchList
        fields = "__all__"