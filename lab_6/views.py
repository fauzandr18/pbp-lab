from django.shortcuts import render
from lab_3.models import MyWatchList
from .forms import WatchlistForm
from django.http.response import HttpResponse
from django.core import serializers
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.shortcuts import (get_object_or_404,
                              render)

from lab_3.views import json
from .forms import WatchlistForm
# import requests

# Create your views here.
def index(request):
    WatchLists = MyWatchList.objects.all()
    response = {'WatchLists': WatchLists}
    return render(request, 'lab6_index.html', response)

def get_watchlist(request, id):
    data = serializers.serialize('json', MyWatchList.objects.filter(pk=id))
    return HttpResponse(data, content_type="application/json")

def update_movie(request, id):
    if request.method == 'POST':
        # fetch the object related to passed id
        obj = get_object_or_404(MyWatchList, id = id)
    
        # pass the object as instance in form
        form = WatchlistForm(request.POST or None, request.FILES, instance = obj)
        # print(request.POST['image_content'])
        # check if form data is valid
        if form.is_valid():
            print("form is valid")
                # save the form data to model
            form.save()
            print("form is saved")
            return HttpResponse(form)
        form = WatchlistForm()
        response = {'form': form}
        print("end point")
        return HttpResponse(response)


    # dictionary for initial data with
    # field names as keys
    context ={}

 
    # save the data from the form and
    # redirect to detail_view
    if form.is_valid():
        print("form is valid 2")
        form.save()
        return HttpResponse(form)
 
    # add form dictionary to context
    context["form"] = form
 
    # return render(request, "update_view.html", context)

# delete view for details
def delete_movie(request, id):
    # dictionary for initial data with
    # field names as keys
    context ={}
 
    # fetch the object related to passed id
    obj = get_object_or_404(MyWatchList, id = id)
 
    print("delete request")
    if request.method =="POST":
        # delete object
        obj.delete()
        print("object deleted")
        # after deleting redirect to
        # home page
        return HttpResponse({})
 
    return HttpResponse({})