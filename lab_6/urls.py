from django.urls import path
from .views import delete_movie, get_watchlist, index, update_movie, delete_movie
from django.conf.urls.static import static
from praktikum import settings


urlpatterns = [
    path('', index, name='index'),
    path('watch-list/<id>', get_watchlist),
    path('watch-list/<id>/update', update_movie),
    path('watch-list/<id>/delete', delete_movie),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)