# Generated by Django 3.2.7 on 2022-06-21 04:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_3', '0004_remove_mywatchlist_image_content'),
    ]

    operations = [
        migrations.AddField(
            model_name='mywatchlist',
            name='image_content',
            field=models.ImageField(null=True, upload_to='lab5-images'),
        ),
    ]
