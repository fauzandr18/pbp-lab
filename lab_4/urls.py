from django.urls import path
from .views import index, add_tugas

urlpatterns = [
    path('', index, name='index'),
    path('add/', add_tugas)
]
