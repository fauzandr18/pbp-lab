from sqlite3 import Date
from django import forms
from lab_2.models import TrackerTugas

class TugasForm(forms.ModelForm):
    class Meta:
        model = TrackerTugas
        fields = "__all__"
        # fields = ['deadline', 'course', 'detail']
        widgets = {
            "deadline" : forms.DateInput(),
            "course" : forms.TextInput(attrs={"placeholder" : "Insert course name", }),
            "detail" : forms.Textarea(attrs={"placeholder" : "Insert detail of the task", "rows" : 2, "cols" : 24})
            }