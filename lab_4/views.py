from django.shortcuts import render
from django.http import HttpResponse
from lab_2.models import TrackerTugas
from .forms import TugasForm
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect

# Create your views here.
@login_required(login_url="/admin/login/")
def index(request):
    list_tugas = TrackerTugas.objects.all() 
    response = {
        'list_tugas' : list_tugas
        }
    return render(request, 'lab4_index.html', response)

def add_tugas(request):
    tugas_form = TugasForm(request.POST or None)
    if request.method == 'POST' :
        if tugas_form.is_valid():
            tugas_form.save()
            return redirect('/lab-4/')
    my_name = "Fauzan Daffa R"
    response = {
        'tugas_form' : tugas_form,
        'name' : my_name
        }
    return render(request, 'lab4_form.html', response)