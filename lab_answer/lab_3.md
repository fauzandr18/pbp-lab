\##### 1. Apakah perbedaan antara JSON dan XML?

XML atau eXtensible Markup Language didesain untuk menyimpan data. 

XML tidak mempunyai tipe. 

XML menyediakan *namespace*.  

ekstensi file .xml. 

XML merupakan *markup language*. 

XML dibuat pada tahun 1996. 

XML tidak mendukung *array*. 

XML mendukung teks, angka, gambar, *charts*, *graphs*, dll. 



JSON atau  JavaScript Object Notation didesain agar dapat mudah dibaca oleh manusia.

JSON memiliki banyak tipe.

JSON tidak menyediakan  *namespace*.

ekstensi file .json

JSON dibuat pada tahun 2002.

JSON mendukung *array*.

JSON hanya mendukung tipe data teks dan angka.



\##### 2. Apakah perbedaan antara HTML dan XML?

XML berfokus pada transfer data sedangkan HTML berfokus pada presentasi data.

XML *content driven* sedangkan HTML *format driven*.

XML *case sensitive* sedangkan HTML *case insensitive*.

XML mendukung *namespace* HTML tidak support *namespace*

XML tags *extensible* sedangkan HTML tags terbatas.

