from django.shortcuts import redirect, render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from .models import MyWatchLizt
from .forms import WatchlistForm
from django.contrib.auth.decorators import login_required


# Create your views here.
def index(request):
    WatchLists = MyWatchLizt.objects.all() 
    response = {'WatchLists': WatchLists}
    return render(request, 'lab5_index.html', response)

def myWatchList(request):
    WatchLists = MyWatchLizt.objects.all() 
    response = {'WatchLists': WatchLists}
    return render(request, 'lab5_watch_list.html', response)

@login_required(login_url='/admin/login/')
def add_watchlist(request):
  if request.method == 'POST':
    print("request method is POST")
    form = WatchlistForm(request.POST,request.FILES)
    print(form)
    print (form.is_valid())
    print(form.errors)
    print(request.POST['watched'])
    print(request.POST['title'])
    print(request.POST['rating'])
    print(request.POST['release_date'])
    print(request.POST['review'])
    # print(request.POST['image_content'])
    # check if form data is valid
    if form.is_valid():
      print("form is valid")
        # save the form data to model
      form.save()
      print("form is saved")
      return redirect('/lab-5/watch-list')
  form = WatchlistForm()
  response = {'form': form}
  return render(request, "lab5_form.html", response)