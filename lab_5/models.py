from contextlib import nullcontext
from tkinter import Widget
from django.db import models
from pkg_resources import require
#from media_lab5-images import default.jpg

# Create your models here.
class MyWatchLizt(models.Model):
    watched = models.BooleanField(default=False)
    title = models.CharField(max_length=100)
    rating = models.FloatField(default="0.0")
    release_date = models.DateField(default="YYYY-MM-DD")
    review = models.TextField() 
    image_content = models.ImageField(upload_to='lab5-images', null=True)
