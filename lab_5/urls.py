from django.urls import path
from .views import index, myWatchList, add_watchlist
from django.conf.urls.static import static
from praktikum import settings


urlpatterns = [
    path('', index, name='index'),
    path('watch-list/', myWatchList),
    path('add_watchlist/', add_watchlist)
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)