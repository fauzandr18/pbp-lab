from django import forms
from .models import MyWatchLizt

class WatchlistForm(forms.ModelForm):
    class Meta:
        model = MyWatchLizt
        fields = "__all__"
        # widgets = {
        #     "watched" : forms.CheckboxInput(), 
        #     "title" : forms.TextInput(attrs={'required':'True', 'max_length':'100'}),
        #     "rating" : forms.NumberInput(attrs={'required':'True'}),
        #     "release_date" : forms.DateInput(attrs={'class':'form-control', 'type':'date', 'required':'True'}),
        #     "review" : forms.TextInput(attrs={'required':'True', 'max_length':'255'}),
            #"image_content" : forms.ImageField()
            #"deadline" : forms.DateInput()
            #"course" : forms.TextInput(attrs={"placeholder" : "Insert course name", })
            #"detail" : forms.Textarea(attrs={"placeholder" : "Insert detail of the task", "rows" : 2, "cols" : 24})
            # }