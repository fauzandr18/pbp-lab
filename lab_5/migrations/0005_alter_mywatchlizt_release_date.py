# Generated by Django 3.2.7 on 2022-06-21 11:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lab_5', '0004_mywatchlizt_image_content'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mywatchlizt',
            name='release_date',
            field=models.DateField(default='YYYY-MM-DD'),
        ),
    ]
