from django.db import models
from django.utils import timezone

# TODO Create TrackerTugas model that contains course, detail, and deadline (date) here


class TrackerTugas(models.Model):
    course = models.CharField(max_length=30)
    detail = models.CharField(max_length=255, default="")
    deadline = models.DateTimeField(max_length=30, default=timezone.now)
    # TODO Implement missing attributes in TrackerTugas model
