import 'package:flutter/material.dart';

import '../models/player.dart';
import '../widgets/player_item.dart';

class FavoritesScreen extends StatelessWidget {
  final List<Player> favoritePlayers;

  FavoritesScreen(this.favoritePlayers);

  @override
  Widget build(BuildContext context) {
    if (favoritePlayers.isEmpty) {
      return Center(
        child: Text('Belum ada yang dipilih.'),
      );
    } else {
      return ListView.builder(
        itemBuilder: (ctx, index) {
          return PlayerItem(
            id: favoritePlayers[index].id,
            title: favoritePlayers[index].title,
            imageUrl: favoritePlayers[index].imageUrl,
          );
        },
        itemCount: favoritePlayers.length,
      );
    }
  }
}
