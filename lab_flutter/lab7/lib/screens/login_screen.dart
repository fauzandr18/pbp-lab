import 'package:flutter/material.dart';
import '../widgets/main_drawer.dart';

class LoginScreen extends StatelessWidget {
  //
  static const routeName = '/quiz-enter-screen';

  //
  @override
  Widget build(BuildContext ctx) {
    return Scaffold(
      //
      appBar: AppBar(
        title: Text("Welcome!"),
      ),

      //
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            //
            Text("Welcome. Feel free to log in."),

            //
            ElevatedButton(
              child: Text('Login'),
              onPressed: () {
                //
              },
            ),

            //
            Text("Don't have an account yet? Register here!"),

            //
            ElevatedButton(
              child: Text('Register'),
              onPressed: () {
                //
              },
            ),
          ]
        )
      ),

      //
      drawer: MainDrawer(),
    );
  }
}