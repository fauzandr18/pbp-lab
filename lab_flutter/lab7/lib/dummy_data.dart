import 'package:flutter/material.dart';

import './models/category.dart';
import './models/meal.dart';

import 'package:flutter/material.dart';

import './models/category.dart';
import './models/player.dart';

const DUMMY_CATEGORIES = const [
  Category(
    id: 'c1',
    title: 'Forward',
    color: Colors.purple,
  ),
  Category(
    id: 'c2',
    title: 'Midfielder',
    color: Colors.red,
  ),
  Category(
    id: 'c3',
    title: 'Defender',
    color: Colors.orange,
  ),
  Category(
    id: 'c4',
    title: 'Goalkeeper',
    color: Colors.amber,
  ),
];

const DUMMY_PLAYERS = const [
  Player(
    id: 'm1',
    categories: [
      'c1',
      'c3',
    ],
    title: 'Cristiano Ronaldo',
    imageUrl:
        'https://assets.pikiran-rakyat.com/crop/151x0:1903x1204/x/photo/2022/05/14/1149393770.jpg',
    description: 'Cristiano Ronaldo dos Santos Aveiro adalah seorang pemain sepak bola profesional asal Portugal yang bermain sebagai penyerang untuk klub Liga Inggris, Manchester United, dan juga kapten untuk tim nasional Portugal.',
  ),
  Player(
    id: 'm2',
    categories: [
      'c1',
      'c2',
    ],
    title: 'Lionel Messi',
    imageUrl:
        'https://assets.goal.com/v3/assets/bltcc7a7ffd2fbf71f5/bltee41eef7e0a0e3f3/626b8172918f4e4e59121e78/GettyImages-1240053215.jpg',
    description: 'Lionel Andrés Messi juga dikenal sebagai Leo Messi, adalah seorang pemain sepak bola profesional asal Argentina yang bermain sebagai penyerang untuk klub Ligue 1 Paris Saint-Germain dan merupakan kapten tim nasional Argentina.',
  ),
  Player(
    id: 'm3',
    categories: [
      'c1',
      'c2',
      'c3',
    ],
    title: 'Neymar',
    imageUrl:
        'https://idsb.tmgrup.com.tr/ly/uploads/images/2021/05/08/113689.jpg',
    description: 'Neymar da Silva Santos Júnior yang umumnya dikenal sebagai Neymar atau Neymar Jr, adalah pemain sepak bola profesional Brasil yang bermain untuk klub Prancis Paris Saint-Germain. Dia bermain sebagai penyerang atau pemain sayap.',
  ),
  Player(
    id: 'm4',
    categories: [
      'c4',
    ],
    title: 'Manuel Neuer',
    imageUrl:
        'https://awsimages.detik.net.id/community/media/visual/2022/03/28/manuel-neuer-4.jpeg?w=1200',
    description: 'Manuel Peter Neuer merupakan seorang pemain sepak bola Jerman, yang berposisi sebagai penjaga gawang. Saat ini ia bermain untuk Bayern München. Ia bermain untuk tim nasional Jerman.',
  ),
  Player(
    id: 'm5',
    categories: [
      'c4',
    ],
    title: 'Alisson Becker',
    imageUrl:
        'https://cdn.vox-cdn.com/thumbor/yP9oK3ybtFYkdHMjoPTemhETmmc=/0x229:1594x2964/1400x933/filters:focal(1015x435:1341x761):no_upscale()/cdn.vox-cdn.com/uploads/chorus_image/image/70192087/1355390030.0.jpg',
    description: 'Alisson Becker adalah seorang pemain sepak bola berkewarganegaraan Brasil yang bermain untuk klub Liverpool F.C. pada posisi Penjaga gawang. Alisson Becker bermain untuk klub Liverpool sejak tahun 2018, dan membela tim nasional Brasil sejak tahun 2015.',
  ),
];
