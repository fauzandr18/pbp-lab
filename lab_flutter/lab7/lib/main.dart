import 'package:flutter/material.dart';

import './dummy_data.dart';

import './screens/tabs_screen.dart';
import './screens/player_detail_screen.dart';
import './screens/category_players_screen.dart';
import './screens/filters_screen.dart';
import './screens/categories_screen.dart';
import './screens/login_screen.dart';

import './models/player.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  List<Player> _availablePlayers = DUMMY_PLAYERS;
  List<Player> _favoritePlayers = [];

  void _setFilters(Map<String, bool> filterData) {
    setState(() {
      _availablePlayers = DUMMY_PLAYERS.toList();
    });
  }

  void _toggleFavorite(String playerId) {
    final existingIndex = _favoritePlayers.indexWhere((player) => player.id == playerId);

    if (existingIndex >= 0) {
      setState(() {
        _favoritePlayers.removeAt(existingIndex);
      });
    } else {
      setState(() {
        _favoritePlayers.add(
          DUMMY_PLAYERS.firstWhere((player) => player.id == playerId),
        );
      });
    }
  }

  bool _isPlayerFavorite(String id) {
    return _favoritePlayers.any((player) => player.id == id);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab 7',
      theme: ThemeData(
        canvasColor: Color.fromRGBO(255, 254, 229, 1),
        fontFamily: 'Raleway',
        textTheme: ThemeData.light().textTheme.copyWith(
            bodyText1: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            bodyText2: TextStyle(
              color: Color.fromRGBO(20, 51, 51, 1),
            ),
            headline6: TextStyle(
              fontSize: 20,
              fontFamily: 'RobotoCondensed',
              fontWeight: FontWeight.bold,
            )), colorScheme: ColorScheme.fromSwatch(primarySwatch: Colors.blue).copyWith(secondary: Colors.indigo),
      ),
      initialRoute: '/',
      routes: {
        '/': (ctx) => LoginScreen(),
        
        TabsScreen.routeName: (ctx) => TabsScreen(),
        CategoryPlayersScreen.routeName: (ctx) => CategoryPlayersScreen(_availablePlayers),
        PlayerDetailScreen.routeName: (ctx) => PlayerDetailScreen(_toggleFavorite, _isPlayerFavorite),
        
      },
    );
  }
}
