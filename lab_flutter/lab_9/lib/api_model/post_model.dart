import 'dart:convert';

List<Post> postFromJson(String str) =>
    List<Post>.from(json.decode(str).map((x) => Post.fromMap(x)));

class Post {
  Post({
    required this.userId,
    required this.id,
    required this.title,
    required this.review,
    required this.releaseDate,
  });

  int userId;
  int id;
  String title;
  String review;
  String releaseDate;

  factory Post.fromMap(Map<String, dynamic> json) => Post(
        userId: json["userId"],
        id: json["id"],
        title: json["title"],
        review: json["review"],
        releaseDate: json["release_date"],
      );
}