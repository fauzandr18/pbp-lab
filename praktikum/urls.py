"""praktikum URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from django.contrib import admin
from django.http import HttpResponse
import lab_2.urls as lab_2
import lab_3.urls as lab_3
import lab_4.urls as lab_4
import lab_5.urls as lab_5
import lab_6.urls as lab_6
from django.conf.urls.static import static
from . import settings


# method view
def index(request):
	return HttpResponse("Hello world!")

def welcome(request,name):
	return HttpResponse("Welcome, " + name + "!")

def about_me(request, name, hobby):
    return HttpResponse("Hello my name is " + name + ". " + "My hobby is " + hobby +".")

def jumlah(request, a, b):
    return HttpResponse(f"{a + b}")

# TODO Create a new path that redirects to the aboutme page like welcome page (Show your name and your Hobby)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', index, name='index'),
    path('welcome/<str:name>', welcome, name='welcome'),
    path('aboutme/<str:name>/<str:hobby>', about_me),
    path('tambah/<int:a>/<int:b>', jumlah),
    path('lab-2/', include(lab_2)),
    path('lab-3/', include(lab_3)),
    path('lab-4/', include(lab_4)),
    path('lab-5/', include(lab_5)),
    path('lab-6/', include(lab_6)),

    # TODO Add a new path that redirects to the aboutme page 
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
